<?php

namespace Zmz\Messages\Application\Providers;

use Illuminate\Support\ServiceProvider;

class MessageServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom('zmz/Messages/Infrastructure/Migrations');
    }

    public function register()
    {
    }
}
