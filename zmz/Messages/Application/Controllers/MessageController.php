<?php

namespace Zmz\Messages\Application\Controllers;

use App\Http\Controllers\Controller;
use Zmz\Messages\Application\Requests\StoreMessageHttpRequest;
use Zmz\Messages\Application\Resources\MessageResource;
use Zmz\Messages\Domain\Services\MessageService;

class MessageController extends Controller
{
    private $service;

    public function __construct(MessageService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $messsages = $this->service->getAll();
        return MessageResource::collection($messsages);
    }

    public function show(int $message_id)
    {
        $messsage = $this->service->findById($message_id);
        return new MessageResource($messsage);
    }

    public function store(StoreMessageHttpRequest $request)
    {
        $messsage = $this->service->store($request->toArray());
        return new MessageResource($messsage);
    }

    public function update()
    {
    }

    public function destroy(int $message_id)
    {
    }
}
