<?php

namespace Zmz\Messages\Infrastructure\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Zmz\Messages\Domain\Message;

class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sender' => $this->faker->name,
            'recipient' => $this->faker->name,
            'content' => $this->faker->paragraph,
        ];
    }
}
