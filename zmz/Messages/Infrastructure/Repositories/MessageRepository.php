<?php

namespace Zmz\Messages\Infrastructure\Repositories;

use Zmz\Messages\Domain\Message;

class MessageRepository
{
    public function getAll()
    {
        return Message::all();
    }

    public function findById(int $id)
    {
        return Message::findOrFail($id);
    }

    public function store(array $data)
    {
        return Message::create($data);
    }
}
