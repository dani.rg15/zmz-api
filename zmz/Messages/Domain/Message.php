<?php

namespace Zmz\Messages\Domain;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Zmz\Messages\Infrastructure\Factories\MessageFactory;

class Message extends Model
{
    use HasFactory;

    protected $fillable = ['sender', 'recipient', 'content'];

    protected static function newFactory()
    {
        return MessageFactory::new();
    }
}
