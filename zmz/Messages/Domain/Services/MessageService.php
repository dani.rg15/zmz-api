<?php

namespace Zmz\Messages\Domain\Services;

use Zmz\Messages\Domain\Events\MessageCreated;
use Zmz\Messages\Infrastructure\Repositories\MessageRepository;

class MessageService
{
    private $repository;

    public function __construct(MessageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function findById(int $message_id)
    {
        return $this->repository->findById($message_id);
    }

    public function store(array $data)
    {
        $message = $this->repository->store($data);

        event(new MessageCreated($message->id));

        return $message;
    }
}
