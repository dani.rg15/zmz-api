<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Zmz\Messages\Domain\Message;

class MessageResourceTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_get_all_messages()
    {
        $messages = Message::factory()->count(2)->create();

        $this->json('GET', "/api/messages")
            ->assertStatus(200)
            ->assertExactJson(['data' => [
                [
                    'id' => $messages[0]->id,
                    'sender' => $messages[0]->sender,
                    'recipient' => $messages[0]->recipient,
                    'content' => $messages[0]->content,
                    'created_at' => (string) $messages[0]->created_at,
                    'updated_at' => (string) $messages[0]->updated_at,
                ],
                [
                    'id' => $messages[1]->id,
                    'sender' => $messages[1]->sender,
                    'recipient' => $messages[1]->recipient,
                    'content' => $messages[1]->content,
                    'created_at' => (string) $messages[1]->created_at,
                    'updated_at' => (string) $messages[1]->updated_at,
                ]
            ]]);
    }

    public function test_get_one_messages()
    {
        $message = Message::factory()->create();

        $this->json('GET', "/api/messages/{$message->id}")
            ->assertStatus(200)
            ->assertExactJson(['data' => [
                'id' => $message->id,
                'sender' => $message->sender,
                'recipient' => $message->recipient,
                'content' => $message->content,
                'created_at' => (string) $message->created_at,
                'updated_at' => (string) $message->updated_at,
            ]]);
    }

    public function test_create_message()
    {
        $response = $this->json('POST', "/api/messages", [
            'sender' => 'Bob',
            'recipient' => 'Anna',
            'content' => 'Hello!'
        ]);

        $message = Message::findOrFail($response->json('data.id'));

        $response->assertStatus(201)->assertExactJson(['data' => [
            'id' => $message->id,
            'sender' => $message->sender,
            'recipient' => $message->recipient,
            'content' => $message->content,
            'created_at' => (string) $message->created_at,
            'updated_at' => (string) $message->updated_at,
        ]]);
    }
}
